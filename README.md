# What is it ?
This repo provides files to configure my servers and desktop machines running under Ubuntu systems.
The customize.sh file allow to set an environment in few click.

# How to use ?
`wget -O customize.sh https://gitlab.com/Nonot/bash-setup/raw/master/customize.sh; chmod +x customize.sh; sudo ./customize.sh`

`sudo` is not required for basic operations

You are free to reuse this project or suggest modifications !

By default sensitive kube context (that should be diplayed red in prompt) are `("admin")`, you should replace it by whatever you prefer in your context. Edit your `~/.bash_custom` search sensitive_context and edit value, it's a bash array (e.g `sensitive_context=('prod' 'run' 'staging')`)