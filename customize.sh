#!/bin/bash

function PROMPT {
  wget https://gitlab.com/Nonot/bash-setup/raw/master/bash_extra?inline=false -O ${HOME}/.bash_custom
  chown $SUDO_USER:$SUDO_USER ${HOME}/.bash_custom
  if [ $(cat $HOME/.bashrc | grep .bash_custom | wc -l) -eq 0 ]; then
    echo ". ~/.bash_custom" >> $HOME/.bashrc
  fi
}

function choose_options {
  # TITLES MUST MATCH WITH FUNCTION NAMES
  TITLES=(
    "PROMPT"
  )

  DESCRIPTIONS=(
    "Customize prompt"
  )

  STATES=(
    ON
    OFF
  )

  HEIGHT_MENU=${#DESCRIPTIONS[@]}
  HEIGH_BOX=$(( $HEIGHT_MENU + 7 ))
  LENGTH=$(( $HEIGHT_MENU - 1 ))
  CMD_WHIP="whiptail --title \"Choose customizations you wish to apply\" --checklist \"Select with spacebar, confirm with enter\" ${HEIGH_BOX} 78 ${HEIGHT_MENU} "
  END_WHIP="3>&1 1>&2 2>&3"

  for i in `seq 0 $LENGTH`
  do
    CMD_WHIP+="'${TITLES[$i]}' '${DESCRIPTIONS[$i]}' ${STATES[$i]} "
  done

  CMD_WHIP+="$END_WHIP"
  results=$(eval $CMD_WHIP)

  for i in $results
  do
    echo -e "\033[0;32m"
    echo "#######################"
    echo "# Start for $i"
    echo -e "#######################\033[0m"
    eval $i
    echo -e "\033[0;32m#######################"
    echo "# End for $i"
    echo "#######################"
    echo -e "\033[0m"
  done

  echo -e "Customizations ended, thank you !"
}

function start {
  choose_options
}

start
